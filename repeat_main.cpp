// Carlos Campana: Nov 2013
// REPEAT C++/C-vanilla: a la LAMMPS style ;-)
// New version created to overcome some limitations of 
// the old (and ugly) fortran version
#include "io_processing.h"
#include "setup.h"
#include "ewald.h"
#include "solver.h"
using namespace std;                            
int main(int narg, char **arg) {

    int grid_points;
    double alpha;

    cube_str* CUBE;
    CUBE = new cube_str;
    // I/O processing of input parameter file
    read_input_file();
    // Reading ESP cube file
    read_cube_file(CUBE);
    // Reading connectivity file
    read_connectivity_file(CUBE);
    // Tabulate VDW radii, electronegativities and hardnesses
    setup_tabulated_arrays(); 
    // Reading RESP data file
    read_RESP_data_file();
    // Computing the real and reciprocal box vectors 
    compute_box_vectors(CUBE);
    // Setting up the ESP grid data
    grid_points = setup_grid_potential(CUBE);
    // Computing reciprocal space parameters
    alpha = init_ewald_data();
    // Set and solve linear system
    set_solver_data(CUBE,grid_points,alpha); 
    // Clean up memory
    release_memory_global(CUBE);
    delete CUBE;

}

